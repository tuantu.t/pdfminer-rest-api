from flask import Flask, request, Response
import pipe
import logging
app = Flask(__name__)

# app.logger.setLevel(logging.DEBUG)

@app.route("/", methods=["POST"])
def hello():
    app.logger.debug(request.data)
    result = pipe.run(request.data)
    app.logger.debug(result)
    return Response(result, mimetype="text/xml")
