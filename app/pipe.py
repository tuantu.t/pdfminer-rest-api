import subprocess
import tempfile

def run(bytes):
    with tempfile.NamedTemporaryFile() as fh:
        fh.write(bytes)
        p=subprocess.run(args=["pdf2txt.py -t xml "+fh.name], input=bytes, capture_output=True, shell=True, check=False)
        print(p.args)
        if p.returncode != 0:
            print("error:", p.stderr.decode("utf-8"))
            raise RuntimeError("Error while running pdf2txt")
        result = p.stdout.decode("utf-8")
        return result

if __name__=="__main__":
    import sys
    input = sys.stdin.buffer.read()
    result = run(input)
    print(result)