FROM python:3-stretch
RUN pip install pdfminer.six flask chardet
ADD app /app
WORKDIR /app
ENV FLASK_APP=app.py
EXPOSE 5000/tcp
CMD flask run --host=0.0.0.0

