#!/bin/bash

curl -i -X POST localhost:5000 \
  --header "Content-Type: application/pdf" \
  --data-binary "@sample.pdf" | egrep "^Date: " -v > output.xml

