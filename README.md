# PDFMiner REST API  in Docker

This repo hosts the code for a Flask based REST API that allows to process PDF files into [PDFMiner](https://github.com/pdfminer/pdfminer.six).

Furthermore, the flask app is dockerized so it's easy to deploy.

## Rest API

The api only has the root route.

It accepts POST requests that should provide the bytes of the pdf in the body.  
It feeds the pdf bytes to PDFMiner's `pdf2txt.py` and returns the xml output in the response.

## How to use the image

The image does not take any configuration.  
It runs on port 5000.

So you just:

```
docker run -p 5000:5000 registry.gitlab.com/tuantu.t/pdfminer-rest-api
```

And you post your pdf files to http://localhost:5000
